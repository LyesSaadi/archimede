#[macro_use] extern crate log;

use discord::{model::Message, Discord};
use clap::{Parser, CommandFactory};

mod commands;
pub mod common;

use commands::Command;

use crate::common::{answer, send_private};

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
#[command(disable_help_flag = true, disable_help_subcommand = true, disable_version_flag = true)]
struct Archimede {
    /// Affiche l'aide
    #[arg(short, long)]
    help: bool,

    /// La commande à exécuter
    #[command(subcommand)]
    command: Option<Command>,
}

impl Archimede {
    pub fn execute(self, msg: Message, discord: &Discord) {
        match self.command {
            Some(command) => {
                debug!("Passing down to the subcommand.");
                if !self.help {
                    command.execute(msg, discord);
                } else {
                    command.help(msg, discord);
                }
            },
            None => {
                debug!("No command. Displaying help.");

                Archimede::help(msg, discord);
            }
        }
    }

    pub fn help(msg: Message, discord: &Discord) {
        let text = format!("```\n{}\n```", Archimede::command().render_help());

        answer(&text, msg, discord);
    }
}

pub fn parse(msg: Message, discord: &discord::Discord) {
    if !msg.content.starts_with("./archimede") {
        return;
    }

    debug!("Parsing message...");

    match Archimede::try_parse_from(msg.content.split_whitespace()) {
        Ok(parsed) => {
            debug!("Parsed.");
            
            parsed.execute(msg, discord);
        },
        Err(err) => {
            error!("Unable to parse : \"{}\" because of an error : {:#?}", msg.content, err);

            send_private("Désolé, je n'ai pas compris votre requête :[.", msg.author.id, discord);
            Archimede::help(msg, discord);
        }
    };
}

