use discord::{Discord, model::{ChannelId, Message, UserId}};
use serde::Deserialize;
use once_cell::sync::Lazy;

#[derive(Deserialize)]
pub struct Config {
    pub token: String,
    pub server: u64,
    pub category: u64
}

pub fn get_config() -> Config {
    let raw_config = include_str!("../config.toml");
    match toml::from_str(raw_config) {
        Ok(ret) => ret,
        Err(err) => {
            error!("Unable to parse the configuration file : {:#?}", err);
            panic!();
        }
    }
}

pub static CONFIG: Lazy<Config> = Lazy::new(|| {
    get_config()
});

pub fn answer(text: &str, msg: Message, discord: &Discord) {
    let channel = match discord.create_private_channel(msg.author.id) {
        Ok(channel) => {
            discord.delete_message(msg.channel_id, msg.id).unwrap_or_else(|e| {
                error!("Unable to delete message {} in {} :\n{:#?}", msg.id, msg.channel_id, e);
            });
            channel.id
        },
        Err(e) => {
            error!("Unable to establish a private channel with user \"{}\" : {:#?}", msg.author.mention(), e);
            send("Due to an unexpected error, I am unable to answer you in your private messages :[.\n", msg.channel_id, discord);
            msg.channel_id
        }
    };

    send(text, channel, discord);
}

pub fn send_private(text: &str, user: UserId, discord: &Discord) {
    match discord.create_private_channel(user) {
        Ok(channel) => {
            send(text, channel.id, discord);
        },
        Err(e) => {
            error!("Unable to establish a private channel with user \"{}\" : {:#?}", user, e);
        }
    };
}

pub fn send(text: &str, channel: ChannelId, discord: &Discord){
    match discord.send_message(channel, text, "", false) {
        Ok(_) => info!("Message sent in {} :\n{}", channel, text),
        Err(e) => error!("Unable to send message in {} :\n{:?}\n\nMessage Content :\n{}", channel, e, text)
    }
}
