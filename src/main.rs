use pretty_env_logger;
#[macro_use] extern crate log;
use discord::Discord;

use archimede::{parse, common::CONFIG};

fn main() {
    pretty_env_logger::init();

    info!("Booting...");

    let discord = match Discord::from_bot_token(&CONFIG.token) {
        Ok(ret) => ret,
        Err(err) => {
            error!("Unable to create a Discord object from the bot token : {:#?}", err);
            panic!();
        }
    };

    let (mut con, _) = match discord.connect() {
        Ok(ret) => ret,
        Err(err) => {
            error!("Unable to connect to Discord : {:#?}", err);
            panic!();
        }
    };

    info!("Listening!");

    loop {
        match con.recv_event() {
            Ok(discord::model::Event::MessageCreate(msg)) => {
                debug!("Received a message : {:#?}", msg);
                parse(msg, &discord);
            },
            Ok(_) => {},
            Err(discord::Error::Closed(code, body)) => {
                error!("Connection closed [Code {:#?}] : {}", code, body);
                return;
            },
            Err(err) => error!("Error : {:#?}", err),
        }
    }
}

