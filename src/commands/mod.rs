use clap::Subcommand;
use discord::{Discord, model::Message};

pub mod add;
pub mod archive;
pub mod create;
pub mod delete;
pub mod pin;
pub mod remove;
pub mod transfer;

#[derive(Subcommand)]
pub enum Command {
    Create(create::Create),
}

impl Command {
    pub fn execute(self, msg: Message, discord: &Discord) {
        debug!("Executing a Command.");

        match self {
            Command::Create(c) => c.execute(msg, discord),
        }
    }

    pub fn help(self, msg: Message, discord: &Discord) {
        debug!("Displaying a Command's help.");

        match self {
            Command::Create(_) => create::Create::help(msg, discord),
        }
    }
}
