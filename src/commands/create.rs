use clap::{CommandFactory, Parser};
use discord::{model::{Message, Channel, PermissionOverwrite, permissions, Permissions, ChannelType, RoleId}, Discord};

use crate::{answer, common::{CONFIG, send_private}};

/// Créer un salon temporaire
#[derive(Parser)]
#[command(disable_help_flag = true, disable_version_flag = true)]
pub struct Create {
    /// Affiche l'aide
    #[arg(short, long)]
    help: bool,

    /// Le nom du salon
    name: Option<String>,

    /// Les utilisateurs du salon
    users: Option<Vec<String>>,
}

impl Create {
    pub fn execute(self, msg: Message, discord: &Discord) {
        debug!("Executing Create.");

        if self.help {
            Self::help(msg, discord);
            return;
        }

        match &self.name {
            None => Self::help(msg, discord),
            Some(name) => {
                debug!("Setting up channel {name}...");
                match discord.create_channel(discord::model::ServerId(CONFIG.server), name, ChannelType::Text, CONFIG.category) {
                    Ok(channel) => {
                        if let Channel::Public(channel) = channel {
                            info!("Channel {} created.", channel.name);

                            if !msg.mention_everyone {
                                debug!("Setting up perms for @everyone...");

                                if let Err(e) = discord.create_permission(channel.id, PermissionOverwrite {
                                    kind: discord::model::PermissionOverwriteType::Role(RoleId(channel.server_id.0)),
                                    allow: Permissions::empty(),
                                    deny: permissions::READ_MESSAGES,
                                }) {
                                    error!("Unable to set permissions for @everyone in {} : {:#?}", channel.id, e);
                                    send_private(
                                        &format!(
                                            "Erreur lors de la privatisation du channel #{} :[, veuillez notifier un administrateur.",
                                            channel.name
                                        ), msg.author.id, discord);
                                }
                            }

                            let mut users = msg.mentions.clone();
                            users.push(msg.author.clone());

                            for user in users {
                                debug!("Setting up perms for {}...", user.id);

                                if let Err(e) = discord.create_permission(channel.id, PermissionOverwrite {
                                    kind: discord::model::PermissionOverwriteType::Member(user.id),
                                    allow: permissions::READ_MESSAGES,
                                    deny: Permissions::empty(),
                                }) {
                                    error!("Unable to set permissions for {} in {} : {:#?}", user.id, channel.id, e);
                                    send_private(
                                        &format!(
                                            "Impossible d'ajouter {} dans #{} :[, veuillez notifier un administrateur.",
                                            user.mention(),
                                            channel.name
                                        ), msg.author.id, discord);
                                    send_private(
                                        &format!(
                                            "Impossible de vous ajouter dans le salon #{} de {}, veuillez notifier un administrateur.",
                                            channel.name,
                                            msg.author.mention()
                                        ), user.id, discord);
                                }
                            }

                            for role in msg.mention_roles.clone() {
                                debug!("Setting up perms for {}...", role);

                                if let Err(e) = discord.create_permission(channel.id, PermissionOverwrite {
                                    kind: discord::model::PermissionOverwriteType::Role(role),
                                    allow: permissions::READ_MESSAGES,
                                    deny: Permissions::empty(),
                                }) {
                                    error!("Unable to set permissions for {} in {} : {:#?}", role, channel.id, e);
                                    send_private(
                                        &format!(
                                            "Impossible d'ajouter le rôle {} dans #{} :[, veuillez notifier un administrateur.",
                                            role.mention(),
                                            channel.name
                                        ), msg.author.id, discord);
                                }
                            }

                            answer(&format!("Channel #{} créé !", channel.name), msg, discord);
                        }
                    },
                    Err(e) => {
                        answer(&format!("Désolé, je n'ai pas réussi à créer le channel \"{name}\" :[."), msg, discord);
                        error!("Unable to create channel {name} :\n{:#?}", e);
                    }
                }
            }
        }
    }

    pub fn help(msg: Message, discord: &Discord) {
        debug!("Displaying Create's help.");

        let text = format!("```\n{}\n```", Create::command().render_help());

        answer(&text, msg, discord);
    }
}
